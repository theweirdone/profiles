<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use App\Http\Requests;

class ProfileController extends Controller
{

    /**
     * Display user's profiles.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        $profiles = Profile::orderBy('created_at', 'asc')->get();
        return view('profiles.index', [
            'profiles' => $profiles,
        ]);
    }

    /**
     * Create a new profile.
     *
     * @param  Request  $request
     * @return Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'name'                      => 'required|max:255',
            'dob'                       => 'required|date',
            'social_security_number'    => 'required|max:40',
            'address'                   => 'required|max:255',
        ]);

        // Create The Profile...
        $profile = new Profile;

        $profile->name = $request->name;
        $profile->dob = $request->dob;
        $profile->social_security_number = $request->social_security_number;
        $profile->address = $request->address;

        $profile->save();

        return redirect('/profiles');
    }
}
