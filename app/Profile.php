<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    private $encryptor;

    protected $fillable = ['name', 'dob', 'social_security_number', 'address'];

    public function getDobAttribute($value) {
        return date('jS M, Y', strtotime($value));
    }

    // get decrypted social security number
    public function getSocialSecurityNumberAttribute($value) {
        $this->getEncrypter($this->attributes['private_key']);

        return $this->encryptor->decrypt($value);
    }

    // encrypt social security number (and store private key)
    public function setSocialSecurityNumberAttribute($value) {
        $privateKey = $this->createPrivateKey();
        $this->getEncrypter($privateKey);

        $this->attributes['private_key'] = $privateKey;
        $this->attributes['social_security_number'] = $this->encryptor->encrypt($value);
    }

    private function getEncrypter($privateKey) {
        if (!isset($this->encrypter)) {
            $otherKey = env('APP_KEY', "argllsnlaungl") . $privateKey;

            $otherKey = substr(sha1($otherKey), 0, 32);

            $this->encryptor = new \Illuminate\Encryption\Encrypter($otherKey, config('app.cipher'));
        }
    }

    public function createPrivateKey() {
        $uuid = array(
            'time_low'  => 0,
            'time_mid'  => 0,
            'time_hi'  => 0,
            'clock_seq_hi' => 0,
            'clock_seq_low' => 0,
            'node'   => array()
        );

        $uuid['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
        $uuid['time_mid'] = mt_rand(0, 0xffff);
        $uuid['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
        $uuid['clock_seq_hi'] = (1 << 7) | (mt_rand(0, 128));
        $uuid['clock_seq_low'] = mt_rand(0, 255);

        for ($i = 0; $i < 6; $i++) {
            $uuid['node'][$i] = mt_rand(0, 255);
        }

        $uuid = sprintf('%08x%04x%04x%02x%02x%02x%02x%02x%02x%02x%02x',
            $uuid['time_low'],
            $uuid['time_mid'],
            $uuid['time_hi'],
            $uuid['clock_seq_hi'],
            $uuid['clock_seq_low'],
            $uuid['node'][0],
            $uuid['node'][1],
            $uuid['node'][2],
            $uuid['node'][3],
            $uuid['node'][4],
            $uuid['node'][5]
        );

        return $uuid;
    }
    
}
