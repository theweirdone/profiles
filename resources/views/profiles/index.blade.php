@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Profiles
                        </div>
                        <div class="panel-body">
                            <!-- Display Validation Errors -->
                            {{--@include('common.errors')--}}

                            <form action="{{ url('profile') }}" method="POST" class="form-horizontal">
                                {!! csrf_field() !!}

                                <!-- Profile Name -->
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" name="name" id="profile-name" class="form-control">
                                    </div>
                                </div>

                                <!-- Profile Date of Birth -->
                                <div class="form-group">
                                    <label for="dob" class="col-sm-3 control-label">Date of Birth</label>

                                    <div class="col-sm-6">
                                        <input type="date" name="dob" id="profile-dob" class="form-control">
                                    </div>
                                </div>

                                <!-- Profile Social Security Number -->
                                <div class="form-group">
                                    <label for="social_security_number" class="col-sm-3 control-label">Social Security Number</label>

                                    <div class="col-sm-6">
                                        <input type="text" name="social_security_number" id="profile-social-security-number" class="form-control">
                                    </div>
                                </div>

                                <!-- Profile Address -->
                                <div class="form-group">
                                    <label for="address" class="col-sm-3 control-label">Address</label>

                                    <div class="col-sm-6">
                                        {{--<input type="textarea" name="name" id="profile-address" class="form-control">--}}
                                        <textarea name="address" id="profile-address" cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                </div>

                                <!-- Add Task Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fa fa-plus"></i> Add Profile
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @if (count($profiles) > 0)
                        <div class="panel-body">
                            <table class="table table-striped task-table">

                                <!-- Table Headings -->
                                <thead>
                                <th>Name</th>
                                <th>Date of Birth</th>
                                <th>Social Security Number</th>
                                <th>Address</th>
                                </thead>

                                <!-- Table Body -->
                                <tbody>
                                @foreach ($profiles as $profile)
                                    <tr>

                                        <td class="table-text">
                                            <div>{{ $profile->name }}</div>
                                        </td>

                                        <td class="table-text">
                                            <div>{{ $profile->dob }}</div>
                                        </td>

                                        <td class="table-text">
                                            <div>{{ $profile->social_security_number }}</div>
                                        </td>

                                        <td class="table-text">
                                            <div>{{ $profile->address }}</div>
                                        </td>

                                        <td>
                                            <!-- TODO: Delete Button -->
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif
                    </div>
            </div>
        </div>
    </div>
@endsection
